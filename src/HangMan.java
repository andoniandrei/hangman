import java.util.Scanner;

public class HangMan {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);


        // Constante

        // Variabile
        int totalMistake = 0;
        boolean b = false;
        // Codul programului


        showWelcome();
        Words word = new Words();
        System.out.println("Indiciul tau este: " + word.getClue() + ". Daca gresesti 4 litere .... e Game Over! :D");
        KeyboardInput input = new KeyboardInput();
        while (totalMistake < 4 && !word.isSolved()) {
            System.out.println(word);
            char c = input.getNewLetter();
            if (!word.update(c))
                totalMistake = totalMistake + 1;
        }
        if (word.isSolved())
            System.out.println("Felicitari, ai ghicit cuvantul");
        else
            System.out.println("Cum sa iti spun eu... Game Over! :(");

    }

    public static void showWelcome() {
        System.out.println("Ghiceste cuvantul!! :D");
    }

}