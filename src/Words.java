public class Words {
    String clue, visible, secret;

    public Words() {
        String[] wordList = {"masina", "pisica", "pui de piropopircanita", "java"};

        String[] clueList = {"BMW", "Animal", ":))", "SDA"};
        int rand;
        rand = (int) (Math.random() * 4);
        secret = wordList[rand];
        clue = clueList[rand];
        int wordLength = secret.length();

        visible = "";

        for (int i = 0; i < secret.length(); i++) {
            char c = secret.charAt(i);

            if (c == ' ')
                visible += c;
            else
                visible += '_';
        }
    }

    public String getClue() {
        return clue;
    }



    public String toString() {
//         String s = "";
//         for(int i = 0; i < ; i++)
//            s = s + "_ ";
//         return s;

        return visible;
    }

    public boolean update(char c) {
        char[] secretArray = new char[secret.length()];
        char[] visibleArray = new char[secret.length()];
        String s = "";
        boolean contains = false;

        //Facem din stringul "secret" un array
        for (int i = 0; i < secret.length(); i++) {
            secretArray[i] = secret.charAt(i);
            visibleArray[i] = visible.charAt(i);
        }

        //updatam array-ul
        for (int k = 0; k < secret.length(); k++) {

            if (secretArray[k] == c) {
                visibleArray[k] = c;
                contains = true;
            }
        }
        //transformam array-ul in string
        for (int m = 0; m < secret.length(); m++) {
            s = s + visibleArray[m];
        }
        visible = s;

        return contains;
    }

    //Verifica daca s-a gasit solutia
    public boolean isSolved() {
        return secret.equalsIgnoreCase(visible);
    }
}

