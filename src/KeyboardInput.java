import java.util.ArrayList;
import java.util.Scanner;

public class KeyboardInput {
    // Proprietati
    ArrayList<String> keyList;
    boolean chosen;

    //Constructor
    public KeyboardInput() {
        keyList = new ArrayList<String>();
    }

    // Adauga si stocheaza literele scrise la tastatura la array.
    public void keyChosen(String key) {
        keyList.add(key);
    }

    // Verifica daca a mai fost introdusa litera respectiva
    public boolean hasBeenChosen(String key) {
        chosen = keyList.contains(key);
        return chosen;
    }


    public char getNewLetter() {
        Scanner scan = new Scanner(System.in);

        char a;
        boolean chosen;
        String aTmp;    // folosit pentru a il transforma pe a in string si sa pot verifica daca a mai fost folosit sau nu

        do {
            System.out.println("Tasteaza o litera!");
            a = scan.next().charAt(0);
            aTmp = "" + a;
            chosen = hasBeenChosen(aTmp);
            if (!chosen)
                keyChosen(aTmp);
            else {
                System.out.println("Deja ai folosit aceasta litera. Te rog introdu o noua litera!");

            }


        }
        // Repetam procesul pana cand jucatorul nu mai are vieti.
        while (chosen);

        return a;
    }
}
